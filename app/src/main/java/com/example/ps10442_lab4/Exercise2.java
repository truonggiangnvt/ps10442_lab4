package com.example.ps10442_lab4;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class Exercise2 extends AppCompatActivity {
    Button btnCheck, btnExercise3;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise2);
        btnCheck = findViewById(R.id.btnCheck);
        btnExercise3 = findViewById(R.id.btnExercise3);
        setBtnCheck();
        setBtnExercise3();
    }
    private void setBtnExercise3(){
        btnExercise3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Exercise2.this,Exercise3.class));
            }
        });
    }
    private void setBtnCheck(){
        btnCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkConnet();
            }
        });
    }
    public void checkConnet(){
        ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        boolean isWifiConn = networkInfo.isConnected();
        networkInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
        boolean isMobileconn = networkInfo.isConnected();
        if (isWifiConn){
            Toast.makeText(getApplicationContext(),"The phone connected wifi",Toast.LENGTH_SHORT).show();
        } if (isMobileconn){
            Toast.makeText(getApplicationContext(),"The phone connected 4G", Toast.LENGTH_SHORT).show();
        }
    }
}
